﻿Module Module1

    Sub Main()
        Dim motor1 As New MotorNaftero
        motor1.Arrancar()
        motor1.Acelear()
        motor1.Detener()
        motor1.CargarCombustible()


        Dim motor2 As New MotorDiesel
        motor2.Arrancar()
        motor2.Acelear()
        motor2.Detener()
        motor2.CargarCombustible()


        Dim motor3 As New MotorElectricoAdapter
        motor3.Arrancar()
        motor3.Acelear()
        motor3.Detener()
        motor3.CargarCombustible()

        Console.ReadKey()
    End Sub

End Module
