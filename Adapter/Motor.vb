﻿Public MustInherit Class Motor

    Public MustOverride Sub Acelear()
    Public MustOverride Sub Arrancar()
    Public MustOverride Sub Detener()
    Public MustOverride Sub CargarCombustible()
End Class
