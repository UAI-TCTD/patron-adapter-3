﻿Public Class MotorElectricoAdapter
    Inherits Motor

    Private motorElectrico As New MotorElectrico

    Public Overrides Sub Acelear()
        motorElectrico.Mover()
    End Sub

    Public Overrides Sub Arrancar()

        motorElectrico.Activar()
        motorElectrico.Conectar()
    End Sub

    Public Overrides Sub CargarCombustible()
        motorElectrico.Enchufar()
    End Sub

    Public Overrides Sub Detener()
        motorElectrico.Parar()
    End Sub
End Class
