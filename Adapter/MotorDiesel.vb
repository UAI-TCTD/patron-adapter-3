﻿Public Class MotorDiesel
    Inherits Motor


    Public Overrides Sub Acelear()
        Console.WriteLine("Acelerando motor diesel")
    End Sub

    Public Overrides Sub Arrancar()
        Console.WriteLine("Arrancando motor diesel")
    End Sub

    Public Overrides Sub CargarCombustible()
        Console.WriteLine("Cargando combustible en motor diesel")
    End Sub

    Public Overrides Sub Detener()
        Console.WriteLine("Deteniendo motor diesel")
    End Sub
End Class
