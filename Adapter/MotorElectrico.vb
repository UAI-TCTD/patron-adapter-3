﻿Public Class MotorElectrico
    Private _conectado As Boolean = False
    Private _enchufado As Boolean = False
    Private _activo As Boolean = False
    Private _moviendo As Boolean = False


    Public Sub Conectar()
        If _conectado Then
            Console.WriteLine("Imposible conectar un motor electrico ya conectado!")
        Else
            _conectado = True
            Console.WriteLine("Motor conectado!")
        End If

    End Sub


    Public Sub Activar()
        If Not _conectado Then
            Console.WriteLine("Imposible activar un motor no conectado")
        Else
            _activo = True
            Console.WriteLine("Motor Activo!")
        End If
    End Sub

    Public Sub Mover()

        If _conectado And _activo Then
            _moviendo = True
            Console.WriteLine("moviendo motor")
        Else
            Console.WriteLine("El motor deberá estar conectado y activo!")
        End If
    End Sub


    Public Sub Parar()
        If _moviendo Then
            _moviendo = False
            Console.WriteLine("Parando motor!")
        Else
            Console.WriteLine("Imposible parar un motor que no esté en movimiento!")
        End If
    End Sub

    Public Sub Desconectar()
        If _conectado Then
            Console.WriteLine("Motor desconectado!")
        Else
            Console.WriteLine("Imposible desconectar un motor no conectado!")
        End If
    End Sub

    Public Sub Desactivar()
        If _activo Then
            _activo = False
            Console.WriteLine("Motor desconectado!")
        Else
            Console.WriteLine("Imposible desactivar un motor no activo!")
        End If
    End Sub

    Public Sub Enchufar()
        If Not _activo Then
            Console.WriteLine("Cargando las baterias!")
        Else
            Console.WriteLine("Imposible enchufar un motor activo!")
        End If
    End Sub

End Class
