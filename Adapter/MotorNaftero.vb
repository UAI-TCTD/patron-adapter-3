﻿Public Class MotorNaftero
    Inherits Motor


    Public Overrides Sub Acelear()
        Console.WriteLine("Acelerando motor naftero")
    End Sub

    Public Overrides Sub Arrancar()
        Console.WriteLine("Arrancando motor naftero")
    End Sub

    Public Overrides Sub CargarCombustible()
        Console.WriteLine("Cargando combustible en motor naftero")
    End Sub

    Public Overrides Sub Detener()
        Console.WriteLine("Deteniendo motor naftero")
    End Sub
End Class
